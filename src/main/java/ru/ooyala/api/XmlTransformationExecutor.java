package ru.ooyala.api;

import net.sf.saxon.Transform;

public class XmlTransformationExecutor {

    public static void transform(String sourceXmlPath, String xslPath, String targetXmlPath) {
        sourceXmlPath = XmlTransformationExecutor.class.getResource("/xml/" + sourceXmlPath).getPath();
        xslPath = XmlTransformationExecutor.class.getResource("/xml/" + xslPath).getPath();
        String folderForTargetXml = XmlTransformationExecutor.class.getResource("/xml/").getPath();
        try {
            Transform.main(new String[] {
                    "-t",
                    "-s:" + sourceXmlPath,
                    "-xsl:" + xslPath,
                    "-o:" + folderForTargetXml + targetXmlPath});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
