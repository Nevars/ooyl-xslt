MP-5791
* ImageSize starting from L3 level and ending up L7 (episode/movie)
* Collection has '_c-_' prefix, 
label has '_l-_',
master collection has '_m-_', 
series has '_s-_', 
season has '_se-_'
* Season number. It must be taken from series 
(e.g. South Park 12 serie, we should append S12). 
If there's no any series number, we should
append S1. Otherwise, it must be appended S1.


MP-6025
* Discovery API. There must be  
* OTT dates


MP-6020
* Need take advantage of existing API to ingest closed catption (CC) files after XMLs and renditions were ingested with PM-Workflows.
For new videos, StarHub will upload CC files along with source video files. File names of CC files would be same as the video files with the extension ".xml". For example :
LY0012475444.mpg would be uploaded with LY0012475444.xml