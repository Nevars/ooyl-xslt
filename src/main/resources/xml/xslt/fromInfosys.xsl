<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.ooyala.com/media-platform/manifest"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>
    <xsl:output
            method="xml"
            version="1.0"
            encoding="UTF-8"
            indent="yes"
            cdata-section-elements="value"
    />

    <!--
    GLOBALS
    -->
    <xsl:variable name="posterServiceUrl">http://58.146.131.37/poster/</xsl:variable>
    <xsl:key name="productByID" match="Product" use="@id" />
    <xsl:key name="contentByID" match="Content" use="@id" />
    <xsl:key name="catalogueByID" match="Catalogue//Node" use="@id" />

    <!--
    GENERIC HELPERS
    -->
    <!-- Implementation of date:add -->
    <xsl:template name="calcDate">
        <xsl:param name="date"/>
        <xsl:param name="days"/>
        <xsl:variable name="oldYear"><xsl:value-of select="substring($date,1,4)"/></xsl:variable>
        <xsl:variable name="oldMonth"><xsl:value-of select="substring($date,6,2)"/></xsl:variable>
        <xsl:variable name="oldDay"><xsl:value-of select="substring($date,9,2)"/></xsl:variable>
        <xsl:variable name="oldTime"><xsl:value-of select="substring($date,11,10)"/></xsl:variable>
        <xsl:variable name="newMonth">
            <xsl:choose>
                <xsl:when test="$oldMonth = '01'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">01</xsl:when>
                        <xsl:otherwise>02</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '02'">
                    <xsl:choose>
                        <xsl:when test="((($oldYear mod 4 = 0 and $oldYear mod 100 != 0) or $oldYear mod 400 = 0) and number($oldDay) + $days &lt;= 29) or ($oldYear mod 4 != 0 and number($oldDay) + $days &lt;= 28)">02</xsl:when>
                        <xsl:otherwise>03</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '03'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">03</xsl:when>
                        <xsl:otherwise>04</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '04'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 30">04</xsl:when>
                        <xsl:otherwise>05</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '05'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">05</xsl:when>
                        <xsl:otherwise>06</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '06'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 30">06</xsl:when>
                        <xsl:otherwise>07</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '07'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">07</xsl:when>
                        <xsl:otherwise>08</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '08'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">08</xsl:when>
                        <xsl:otherwise>09</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '09'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 30">09</xsl:when>
                        <xsl:otherwise>10</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '10'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">10</xsl:when>
                        <xsl:otherwise>11</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '11'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 30">11</xsl:when>
                        <xsl:otherwise>12</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '12'">
                    <xsl:choose>
                        <xsl:when test="number($oldDay) + $days &lt;= 31">12</xsl:when>
                        <xsl:otherwise>01</xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="newYear">
            <xsl:choose>
                <xsl:when test="$oldMonth = '12' and (number($oldDay) + $days) &gt;= 31"><xsl:value-of select="number($oldYear) + 1"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$oldYear"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="newDay">
            <xsl:choose>
                <xsl:when test="$oldMonth = '01'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '02'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$oldYear mod 4 = 0">
                                    <xsl:value-of select="number($oldDay) + $days - 29"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="number($oldDay) + $days - 28"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '03'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '04'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 30"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '05'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '06'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 30"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '07'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '08'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '09'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 30"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '10'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '11'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 30"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$oldMonth = '12'">
                    <xsl:choose>
                        <xsl:when test="$newMonth = $oldMonth"><xsl:value-of select="number($oldDay) + $days"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="number($oldDay) + $days - 31"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$newYear"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$newMonth" />
        <xsl:text>-</xsl:text>
        <xsl:if test="$newDay &lt; 10">0</xsl:if><xsl:value-of select="$newDay"/>
        <xsl:value-of select="$oldTime"/>
    </xsl:template>

    <!--  -->
    <xsl:template name="string">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="string-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="$value"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="text">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="text-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="$value"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="boolean">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="boolean-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value>
                <xsl:element name="option-child">
                    <xsl:attribute name="value">
                        <xsl:choose>
                            <xsl:when test="$value = 'true' or $value = 'yes' or $value = '1'">true</xsl:when>
                            <xsl:when test="$value = 'false' or $value = 'no' or $value = '0' or $value = ''">false</xsl:when>
                            <xsl:otherwise>
                                <xsl:message terminate='yes'><xsl:text>Invalid boolean value: </xsl:text><xsl:value-of select="$value"/></xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </xsl:element>
            </value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="integer">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="integer-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="translate($value,translate($value, '0123456789', ''), '')"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="long">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="long-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="$value"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="double">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="double-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="$value"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="time">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="time-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="substring-before($value, 'Z')"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="datetime">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="date-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value><xsl:value-of select="substring-before($value, 'Z')"/></value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="date">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:variable name="dateNoTZ" select="substring-before($value, 'T')"/>
        <xsl:variable name="dateYear" select="substring-before($dateNoTZ, '-')"/>
        <xsl:variable name="dateDay" select="substring-after(substring-after($dateNoTZ, '-'), '-')"/>
        <xsl:variable name="dateMonth" select="substring-before(substring-after($dateNoTZ, '-'), '-')"/>
        <xsl:element name="date-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value>
                <xsl:value-of select="$dateDay"/>
                <xsl:text>-</xsl:text>
                <xsl:choose>
                    <xsl:when test="$dateMonth = 1">Jan</xsl:when>
                    <xsl:when test="$dateMonth = 2">Feb</xsl:when>
                    <xsl:when test="$dateMonth = 3">Mar</xsl:when>
                    <xsl:when test="$dateMonth = 4">Apr</xsl:when>
                    <xsl:when test="$dateMonth = 5">May</xsl:when>
                    <xsl:when test="$dateMonth = 6">Jun</xsl:when>
                    <xsl:when test="$dateMonth = 7">Jul</xsl:when>
                    <xsl:when test="$dateMonth = 8">Aug</xsl:when>
                    <xsl:when test="$dateMonth = 9">Sep</xsl:when>
                    <xsl:when test="$dateMonth = 10">Oct</xsl:when>
                    <xsl:when test="$dateMonth = 11">Nov</xsl:when>
                    <xsl:when test="$dateMonth = 12">Dec</xsl:when>
                    <xsl:otherwise>INVALID</xsl:otherwise>
                </xsl:choose>
                <xsl:text>-</xsl:text>
                <xsl:value-of select="$dateYear"/>
            </value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="single-option">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>
        <xsl:element name="single-option-instance">
            <xsl:attribute name="name"><xsl:value-of select="$name"/></xsl:attribute>
            <value>
                <xsl:element name="option-child">
                    <xsl:attribute name="value"><xsl:value-of select="$value"/></xsl:attribute>
                </xsl:element>
            </value>
        </xsl:element>
    </xsl:template>

    <xsl:template name="path">
        <xsl:for-each select="parent::*">
            <xsl:call-template name="path"/>
        </xsl:for-each>
        <xsl:value-of select="name()"/>
        <xsl:text>/</xsl:text>
    </xsl:template>


    <!--
    perfstarhub HELPERS
    -->
    <xsl:template name="genre-map">
        <xsl:param name="name"/>
        <xsl:param name="value"/>
        <xsl:variable name="lastTwoVal" select="substring-after(substring-after($value, ':'), ':')"/>
        <xsl:variable name="genre" select="substring-before($lastTwoVal, ':')"/>
        <xsl:variable name="subgenre" select="substring-after($lastTwoVal, ':')"/>

        <xsl:choose>
            <xsl:when test="$name='genre'">
                <xsl:choose>
                    <xsl:when test="$genre = '0'">News/Current Affairs</xsl:when>
                    <xsl:when test="$genre = '1'">Movies</xsl:when>
                    <xsl:when test="$genre = '2'">Entertainment</xsl:when>
                    <xsl:when test="$genre = '3'">Kids</xsl:when>
                    <xsl:when test="$genre = '4'">Sports</xsl:when>
                    <xsl:when test="$genre = '5'">Education</xsl:when>
                    <xsl:when test="$genre = '6'">Drama Series</xsl:when>
                    <xsl:when test="$genre = '7'">Infotainment</xsl:when>
                    <xsl:when test="$genre = '8'">Chinese</xsl:when>
                    <xsl:when test="$genre = '9'">Movies</xsl:when>
                    <xsl:when test="$genre = 'A'">Others</xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$name='subgenre'">
                <xsl:if test="$genre != '1'">
                    <xsl:choose>
                        <xsl:when test="$subgenre = '0'">General</xsl:when>
                        <xsl:when test="$subgenre = '1'">Comedy</xsl:when>
                        <xsl:when test="$subgenre = '2'">Drama</xsl:when>
                        <xsl:when test="$subgenre = '3'">Animation</xsl:when>
                        <xsl:when test="$subgenre = '4'">Variety</xsl:when>
                        <xsl:when test="$subgenre = '5'">Music</xsl:when>
                        <xsl:when test="$subgenre = '6'">Specials</xsl:when>
                        <xsl:when test="$subgenre = '7'">Family</xsl:when>
                        <xsl:when test="$subgenre = '8'">Documentary</xsl:when>
                        <xsl:when test="$subgenre = '9'">Magazine</xsl:when>
                        <xsl:when test="$subgenre = 'A'">News</xsl:when>
                        <xsl:when test="$subgenre = 'B'">Reality</xsl:when>
                        <xsl:when test="$subgenre = 'C'">Gameshow</xsl:when>
                        <xsl:when test="$subgenre = 'D'">Talkshow</xsl:when>
                        <xsl:when test="$subgenre = 'E'">Travelogue</xsl:when>
                        <xsl:when test="$subgenre = 'F'">Cultural</xsl:when>
                    </xsl:choose>
                </xsl:if>
                <xsl:if test="$genre = '1'">
                    <xsl:choose>
                        <xsl:when test="$subgenre = '0'">General</xsl:when>
                        <xsl:when test="$subgenre = '1'">Action/Adventure</xsl:when>
                        <xsl:when test="$subgenre = '2'">Thriller/Suspense</xsl:when>
                        <xsl:when test="$subgenre = '3'">Sci-Fi</xsl:when>
                        <xsl:when test="$subgenre = '4'">Comedy</xsl:when>
                        <xsl:when test="$subgenre = '5'">Horror</xsl:when>
                        <xsl:when test="$subgenre = '6'">Romance</xsl:when>
                        <xsl:when test="$subgenre = '7'">Drama</xsl:when>
                        <xsl:when test="$subgenre = '8'">Animation</xsl:when>
                        <xsl:when test="$subgenre = '9'">Family</xsl:when>
                        <xsl:when test="$subgenre = 'A'">Specials</xsl:when>
                        <xsl:when test="$subgenre = 'B'">Period</xsl:when>
                        <xsl:when test="$subgenre = 'C'">Western</xsl:when>
                        <xsl:when test="$subgenre = 'D'">Mystery/Crime</xsl:when>
                        <xsl:when test="$subgenre = 'E'">Musical</xsl:when>
                        <xsl:when test="$subgenre = 'F'">Documentary</xsl:when>
                    </xsl:choose>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="genre">
        <xsl:param name="name"/>
        <xsl:param name="value" select="text()"/>

        <xsl:variable name="firstGenre" select="substring-before($value, ';')"/>
        <xsl:variable name="otherGenres" select="substring-after($value, ';')"/>
        <xsl:choose>
            <xsl:when test="string-length($firstGenre) > 0">
                <xsl:call-template name="genre">
                    <xsl:with-param name="name" select="$name"/>
                    <xsl:with-param name="value" select="$otherGenres"/>
                </xsl:call-template>;
                <xsl:call-template name="genre-map">
                    <xsl:with-param name="name" select="$name"/>
                    <xsl:with-param name="value" select="$firstGenre"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="genre-map">
                    <xsl:with-param name="name" select="$name"/>
                    <xsl:with-param name="value" select="$value"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="lysis-id">
        <xsl:variable name="vod-id" select="@id"/>
        <string-instance name="lysis-id">
            <xsl:choose>
                <xsl:when test="string-length($vod-id) > 0">
                    <value><xsl:value-of select="$vod-id"/></value>
                </xsl:when>
            </xsl:choose>
        </string-instance>
    </xsl:template>

    <xsl:template name="image-url">
        <xsl:param name="url" select="text()"/>
        <xsl:param name="name">ooyala-asset-url</xsl:param>
        <xsl:comment><xsl:value-of select="$url"/></xsl:comment>
        <xsl:variable name="transform1" select="substring-after(translate($url, '\\', '/'), 'ftplysis/logo/')"/>
        <!--xsl:variable name="transform1" select="$url"/-->
        <xsl:variable name="transform2" select="concat($posterServiceUrl, $transform1)"/>
        <xsl:call-template name='string'>
            <xsl:with-param name="name" select="$name"/>
            <xsl:with-param name="value" select="$transform2"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="EpgElement">
        <!--
        Converts perfstarhub metadata fields to the normalized schema

        Note that ImageURL is a special case that should not be
        added here. ImageURL is handled by the "image" named template.
        -->
        <xsl:param name="isEPG" />
        <xsl:if test="$isEPG!=1">
            <xsl:choose>
                <xsl:when test="@key='HDContent'">
                    <xsl:call-template name='boolean'><xsl:with-param name="name">hd-content</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='AdAllowed'">
                    <xsl:call-template name='boolean'><xsl:with-param name="name">ad-allowed</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='DisplayTitle'">
                    <xsl:call-template name='string'><xsl:with-param name="name">name</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='ShortTitle'">
                    <xsl:call-template name='string'><xsl:with-param name="name">short-title</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Description'">
                    <xsl:call-template name='text'><xsl:with-param name="name">description</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='LongDescription'">
                    <xsl:call-template name='text'><xsl:with-param name="name">long-description</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='EpisodeTitle'">
                    <xsl:call-template name='string'><xsl:with-param name="name">episode-title</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Actors'">
                    <xsl:call-template name='string'><xsl:with-param name="name">actors</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Genre'">
                    <xsl:call-template name='string'><xsl:with-param name="name">genre</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='SubGenre'">
                    <xsl:call-template name='string'><xsl:with-param name="name">subgenre</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='AudioMode'">
                    <xsl:call-template name='string'><xsl:with-param name="name">audio-mode</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='EpisodeNumber'">
                    <xsl:if test="floor(text()) = text() or substring(text(),1,2) = 'Ep'">
                        <xsl:call-template name='integer'><xsl:with-param name="name">episode-number</xsl:with-param></xsl:call-template>
                        <xsl:call-template name='integer'><xsl:with-param name="name">ooyala-order</xsl:with-param></xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@key='TotalNumberOfEpisodes'">
                    <xsl:call-template name='integer'><xsl:with-param name="name">total-episodes</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='ParentalRating'">
                    <xsl:call-template name='single-option'><xsl:with-param name="name">parental-rating</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='ProviderId'">
                    <xsl:call-template name='string'><xsl:with-param name="name">provider</xsl:with-param></xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:if>

        <xsl:if test="$isEPG=1">
            <!-- Channel metadata -->
            <xsl:choose>
                <xsl:when test="@key='ProgrammeStatusID'">
                    <xsl:call-template name='string'><xsl:with-param name="name">programme-status-id</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='InternetVod'">
                    <xsl:call-template name="boolean"><xsl:with-param name="name">internet-vod</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Definition'">
                    <xsl:call-template name="string"><xsl:with-param name="name">definition</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='HDChannel'">
                    <xsl:call-template name="boolean"><xsl:with-param name="name">hd-channel</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='DubbedLanguages'">
                    <xsl:call-template name='string'><xsl:with-param name="name">dubbed-languages</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Language'">
                    <xsl:if test="text()!='und'">
                        <xsl:call-template name='single-option'><xsl:with-param name="name">language</xsl:with-param></xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@key='Censorship'">
                    <xsl:call-template name='string'><xsl:with-param name="name">censorship</xsl:with-param></xsl:call-template>
                </xsl:when>
                <!-- <xsl:when test="@key='BlackedOutInternet'">
                  <xsl:call-template name='boolean'><xsl:with-param name="name">is-blackout</xsl:with-param></xsl:call-template>
                </xsl:when> -->
                <xsl:when test="@key='CatchUpInternet'">
                    <xsl:call-template name='boolean'><xsl:with-param name="name">catchup-internet</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='EpisodeNumber'">
                    <xsl:if test="floor(text()) = text() or substring(text(),1,2) = 'Ep'">
                        <xsl:call-template name='integer'><xsl:with-param name="name">episode-number</xsl:with-param></xsl:call-template>
                        <xsl:call-template name='integer'><xsl:with-param name="name">ooyala-order</xsl:with-param></xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@key='ParentalRating'">
                    <xsl:call-template name='single-option'><xsl:with-param name="name">parental-rating</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Genre'">
                    <xsl:call-template name='string'><xsl:with-param name="name">genre</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Description'">
                    <xsl:call-template name='text'><xsl:with-param name="name">description</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='LongDescription'">
                    <xsl:call-template name='text'><xsl:with-param name="name">long-description</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='EpisodeTitle'">
                    <xsl:call-template name='string'><xsl:with-param name="name">episode-title</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Actors'">
                    <xsl:call-template name='string'><xsl:with-param name="name">actors</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='Directors'">
                    <xsl:call-template name='string'><xsl:with-param name="name">directors</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='DisplayTitle'">
                    <xsl:call-template name='string'><xsl:with-param name="name">name</xsl:with-param></xsl:call-template>
                </xsl:when>
                <xsl:when test="@key='ShortDisplayTitle'">
                    <xsl:call-template name='string'><xsl:with-param name="name">short-title</xsl:with-param></xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:if>

    </xsl:template>

    <xsl:template match="EpgDescription">
        <xsl:param name="isEPG" />
        <xsl:choose>
            <xsl:when test="@locale">
                <complex-instance name="display">
                    <value>
                        <xsl:call-template name='single-option'>
                            <xsl:with-param name='name'>display-locale</xsl:with-param>
                            <xsl:with-param name='value' select='@locale'/>
                        </xsl:call-template>
                        <xsl:call-template name="string">
                            <xsl:with-param name="name">content-lysis-id</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:value-of select="../@contentRef"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name='string'>
                            <xsl:with-param name="name">genre</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:call-template name="genre">
                                    <xsl:with-param name="name">genre</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="../EpgDescription/EpgElement[@key='DVB_Content']"/></xsl:with-param>
                                </xsl:call-template>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name='string'>
                            <xsl:with-param name="name">subgenre</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:call-template name="genre">
                                    <xsl:with-param name="name">subgenre</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="../EpgDescription/EpgElement[@key='DVB_Content']"/></xsl:with-param>
                                </xsl:call-template>
                            </xsl:with-param>
                        </xsl:call-template>

                        <xsl:apply-templates select="EpgElement">
                            <xsl:with-param name='isEPG'><xsl:value-of select="$isEPG"/></xsl:with-param>
                        </xsl:apply-templates>
                    </value>
                </complex-instance>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="EpgElement">
                    <xsl:with-param name='isEPG'><xsl:value-of select="$isEPG"/></xsl:with-param>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--
    perfstarhub PRODUCTS
    -->
    <xsl:template name="product-id">
        <xsl:choose>
            <xsl:when test="@type = 'single' and not(Price)">
                <xsl:value-of select="'Free'" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat('PPV_', Price/@rentalDuration, '_', Price/@price)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="Product">
        <xsl:variable name="rental" select="Price/@rentalDuration"/>
        <xsl:variable name="price" select="Price/@price"/>
        <xsl:if test="@type != 'subscription'">
            <xsl:element name="asset">
                <xsl:attribute name="type">product</xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:call-template name="product-id"/>
                </xsl:attribute>
                <groups>
                    <xsl:call-template name="assetGroups">
                        <xsl:with-param name="groupIds" select="@id"/>
                        <xsl:with-param name="groupType">offer</xsl:with-param>
                    </xsl:call-template>
                </groups>

                <metadata-instance name="perfstarhub-product">
                    <value>
                        <xsl:call-template name="string">
                            <xsl:with-param name="name">display-name</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:call-template name="product-id"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="string">
                            <xsl:with-param name="name">product-id</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:call-template name="product-id"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="integer">
                            <xsl:with-param name="name">rental-duration</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:choose>
                                    <xsl:when test="$rental != '' and floor($rental) = $rental">
                                        <xsl:value-of select="$rental"/>
                                    </xsl:when>
                                    <xsl:otherwise>0</xsl:otherwise>
                                </xsl:choose>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="double">
                            <xsl:with-param name="name">price</xsl:with-param>
                            <xsl:with-param name="value">
                                <xsl:choose>
                                    <xsl:when test="$price != ''">
                                        <xsl:value-of select="$price"/>
                                    </xsl:when>
                                    <xsl:otherwise>0</xsl:otherwise>
                                </xsl:choose>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="ooyala-state"/>
                    </value>
                </metadata-instance>
            </xsl:element>
            <xsl:call-template name="offer" />
        </xsl:if>
    </xsl:template>

    <xsl:template name="offer">
        <xsl:element name="asset">
            <xsl:attribute name="type">offer</xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <metadata-instance name="ooyala-offer">
                <value>
                    <!-- <xsl:call-template name="string">
                      <xsl:with-param name="name">name</xsl:with-param>
                      <xsl:with-param name="value" select="@title"/>
                    </xsl:call-template> -->
                    <xsl:call-template name="string">
                        <xsl:with-param name="name">product-id</xsl:with-param>
                        <xsl:with-param name="value">
                            <xsl:call-template name="product-id" />
                        </xsl:with-param>
                    </xsl:call-template>
                    <complex-instance name="availability-window">
                        <value>
                            <xsl:element name="time-instance">
                                <xsl:attribute name="name">availability-window-start</xsl:attribute>
                                <value><xsl:value-of select="substring-before(@startPurchase, 'Z')"/></value>
                            </xsl:element>
                            <xsl:element name="time-instance">
                                <xsl:attribute name="name">availability-window-end</xsl:attribute>
                                <value><xsl:value-of select="substring-before(@endPurchase, 'Z')"/></value>
                            </xsl:element>
                        </value>
                    </complex-instance>
                </value>
            </metadata-instance>
        </xsl:element>
    </xsl:template>

    <!--
    perfstarhub CATALOGUE
    -->
    <xsl:template match="Catalogue">
        <!-- Skip top-level root -->
        <xsl:apply-templates select="Node//Node"/>
    </xsl:template>

    <!-- node template, defaults to collection type -->
    <xsl:template name="catalogue-node">
        <xsl:param name="type">collection</xsl:param>
        <xsl:param name="full">1</xsl:param>
        <xsl:param name="layer2-name"></xsl:param>
        <xsl:param name="parent-name"><xsl:value-of select="parent::*[@title]"/></xsl:param>
        <xsl:param name="grandparent-name"><xsl:value-of select="../../@title"/></xsl:param>
        <xsl:param name="greatgrandparent-name"><xsl:value-of select="../../../@title"/></xsl:param>
        <xsl:param name="node-id"><xsl:value-of select="@id"/></xsl:param>
        <xsl:param name="parent-id"><xsl:value-of select="../@id"/></xsl:param>
        <xsl:param name="grandparent-id"><xsl:value-of select="../../@id"/></xsl:param>
        <xsl:param name="greatgrandparent-id"><xsl:value-of select="../../../@id"/></xsl:param>
        <xsl:param name="title"><xsl:value-of select="@title"/></xsl:param>
        <xsl:param name="addPrefix">1</xsl:param>
        <xsl:variable name="id-prefix">
            <xsl:choose>
                <xsl:when test="$type='series' and $addPrefix='1'">s-</xsl:when>
                <xsl:when test="$type='label' and $addPrefix='1'">l-</xsl:when>
                <xsl:when test="$type='collection' and $addPrefix='1'">c-</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>


            <xsl:when test="$full = 1">
                <xsl:element name="asset">
                    <xsl:attribute name="type"><xsl:value-of select="$type"/></xsl:attribute>
                    <xsl:attribute name="id"><xsl:value-of select="concat($id-prefix,$node-id)"/></xsl:attribute>
                    <xsl:if test="$parent-name != 'root' and $parent-name != $layer2-name and ($type='collection' or $type='series')">
                        <groups>
                            <xsl:call-template name="assetGroups">
                                <xsl:with-param name="groupIds" select="$parent-id"/>
                                <xsl:with-param name="groupType">label</xsl:with-param>
                                <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            </xsl:call-template>
                            <xsl:call-template name="assetGroups">
                                <xsl:with-param name="groupIds" select="$grandparent-id"/>
                                <xsl:with-param name="groupType">collection</xsl:with-param>
                                <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            </xsl:call-template>
                        </groups>
                    </xsl:if>
                    <xsl:if test="$parent-name != 'root' and $parent-name != $layer2-name and ($type='season')">
                        <groups>
                            <xsl:call-template name="assetGroups">
                                <xsl:with-param name="groupIds" select="$parent-id"/>
                                <xsl:with-param name="groupType">series</xsl:with-param>
                                <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            </xsl:call-template>
                            <xsl:call-template name="assetGroups">
                                <xsl:with-param name="groupIds" select="$greatgrandparent-id"/>
                                <xsl:with-param name="groupType">series</xsl:with-param>
                                <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            </xsl:call-template>
                        </groups>
                    </xsl:if>
                    <xsl:element name="metadata-instance">
                        <xsl:attribute name="name">
                            <xsl:choose>
                                <xsl:when test="$type='collection' or $type='label'">ooyala-</xsl:when>
                                <xsl:when test="$type='channel'">ooyala-live-</xsl:when>
                                <xsl:otherwise>perfstarhub-</xsl:otherwise>
                            </xsl:choose><xsl:value-of select="$type"/>
                        </xsl:attribute>
                        <value>
                            <xsl:choose>
                                <xsl:when test="string-length($layer2-name) = 0">
                                    <xsl:call-template name='string'>
                                        <xsl:with-param name='name'>display-name</xsl:with-param>
                                        <xsl:with-param name="value" select="$title"/>
                                    </xsl:call-template>

                                    <xsl:call-template name='string'>
                                        <xsl:with-param name='name'>name</xsl:with-param>
                                        <xsl:with-param name="value" select="$title"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="string-length($layer2-name) != 0">
                                    <xsl:call-template name='string'>
                                        <xsl:with-param name='name'>display-name</xsl:with-param>
                                        <xsl:with-param name="value" select="concat($layer2-name,'_', $title)"/>
                                    </xsl:call-template>
                                    <xsl:call-template name='string'>
                                        <xsl:with-param name='name'>name</xsl:with-param>
                                        <xsl:with-param name="value" select="concat($layer2-name,'_', $title)"/>
                                    </xsl:call-template>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:call-template name='string'>
                                <xsl:with-param name='name'>title-sort-value</xsl:with-param>
                                <xsl:with-param name="value" select="$title"/>
                            </xsl:call-template>
                            <xsl:if test="$type='series'">
                                <xsl:call-template name='string'>
                                    <xsl:with-param name='name'>ooyala-state</xsl:with-param>
                                    <xsl:with-param name="value">published</xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                        </value>
                    </xsl:element>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$type"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Sorry about what you are about to see, but SH's Catalogue structure makes no sense. Exceptions are the rule. Thanks -->

    <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

    <!-- Nodes that should be mapped to collections and labels-->
    <xsl:template match="Catalogue/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'MAS']">
        <xsl:param name="full">1</xsl:param>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">collection</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"></xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">label</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- Nodes that should be mapped to collections and labels-->
    <xsl:template match="Catalogue/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'COL']">
        <xsl:param name="full">1</xsl:param>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">collection</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"><xsl:value-of select="../@title"/></xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">label</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"><xsl:value-of select="../@title"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- Nodes that should be mapped to labels-->
    <xsl:template match="Catalogue/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'LAB']">
        <xsl:param name="full">1</xsl:param>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">label</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"><xsl:value-of select="@title"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- Nodes that should be mapped to Series-->
    <xsl:template match="Catalogue/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'RIS']">
        <xsl:param name="full">1</xsl:param>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">series</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"><xsl:value-of select="@title"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="Catalogue/Node/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'RIS']">
        <xsl:param name="full">1</xsl:param>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">series</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"><xsl:value-of select="@title"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- Nodes that should be mapped to Seasons-->
    <xsl:template match="Catalogue/Node/Node/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'SEA']">
        <xsl:param name="full">1</xsl:param>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">season</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="$full"></xsl:value-of></xsl:with-param>
            <xsl:with-param name="layer2-name"><xsl:value-of select="@title"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- Nodes that don't match one of the above rules -->
    <xsl:template match="Node">
        <xsl:param name="full">1</xsl:param>
    </xsl:template>


    <!-- Nodes ignored -->
    <xsl:template match="Catalogue/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'IGN']" />
    <xsl:template match="Catalogue/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'IGN']" />
    <xsl:template match="Catalogue/Node/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'IGN']" />
    <xsl:template match="Catalogue/Node/Node/Node/Node/Node/Node[EpgDescription/translate(EpgElement[@key='ImageSize'],$smallcase, $uppercase) = 'IGN']" />


    <!--
    perfstarhub ASSETS
    -->
    <xsl:template name="image">
        <xsl:param name="assetId"/>
        <xsl:if test=".//EpgElement[@key='ImageURL'] and string-length($assetId) > 0">
            <xsl:element name="asset">
                <xsl:attribute name="type">image</xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="$assetId"/><xsl:text>.image</xsl:text></xsl:attribute>
                <xsl:attribute name="parent-id"><xsl:value-of select="$assetId"/></xsl:attribute>
                <metadata-instance name="ooyala-image">
                    <value>
                        <xsl:call-template name="image-url">
                            <xsl:with-param name="url" select=".//EpgElement[@key='ImageURL']/text()"/>
                        </xsl:call-template>
                        <string-instance name="ooyala-state">
                            <value>ingested</value>
                        </string-instance>
                    </value>
                </metadata-instance>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template name="Licence">
        <xsl:param name="license-start-time"/>
        <xsl:param name="license-end-time"/>
        <xsl:call-template name='time'>
            <xsl:with-param name="name">license-start-time</xsl:with-param>
            <xsl:with-param name="value" select="$license-start-time"/>
        </xsl:call-template>
        <xsl:call-template name='time'>
            <xsl:with-param name="name">license-end-time</xsl:with-param>
            <xsl:with-param name="value" select="$license-end-time"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="OTT">
        <xsl:param name="start"><xsl:value-of select="Period/@start"/></xsl:param>
        <xsl:param name="end"><xsl:value-of select="Period/@end"/></xsl:param>
        <xsl:call-template name='time'>
            <xsl:with-param name="name">ott-start-date</xsl:with-param>
            <xsl:with-param name="value" select="$start"/>
        </xsl:call-template>
        <xsl:call-template name='time'>
            <xsl:with-param name="name">ott-end-date</xsl:with-param>
            <xsl:with-param name="value" select="$end"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="TechnicalMetadata[@key='Definition']">
        <xsl:call-template name="single-option">
            <xsl:with-param name="name">ooyala-rendition-type</xsl:with-param>
            <xsl:with-param name="value">
                <xsl:choose>
                    <xsl:when test=". = 'VOD-SD'">SD</xsl:when>
                    <xsl:otherwise>HD</xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="ooyala-state">
        <string-instance name="ooyala-state">
            <value>ingesting</value>
        </string-instance>
    </xsl:template>

    <xsl:template match="Media">
        <xsl:param name="assetId"/>
        <xsl:variable name="contentId" select="ancestor::Content/@id"/>
        <xsl:variable name="fileSize" select="TechnicalMetadata[@key='FileSize']"/>
        <xsl:element name="asset">
            <xsl:attribute name="type">rendition</xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="if (substring-after(@fileName, '/') = '') then @fileName else substring-after(@fileName, '/')"/></xsl:attribute>
            <xsl:attribute name="parent-id"><xsl:value-of select="$assetId"/></xsl:attribute>
            <metadata-instance name="ooyala-rendition">
                <value>
                    <xsl:call-template name="long">
                        <xsl:with-param name="name">ooyala-file-size</xsl:with-param>
                        <xsl:with-param name="value">
                            <xsl:choose>
                                <xsl:when test="$fileSize != ''">
                                    <xsl:value-of select="$fileSize"/>
                                </xsl:when>
                                <xsl:otherwise>0</xsl:otherwise>
                            </xsl:choose>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:call-template name="long">
                        <xsl:with-param name="name">ooyala-duration-seconds</xsl:with-param>
                        <xsl:with-param name="value" select="@frameDuration"/>
                    </xsl:call-template>
                    <xsl:call-template name="string">
                        <xsl:with-param name="name">ooyala-asset-url</xsl:with-param>
                        <xsl:with-param name="value"><xsl:text>file://</xsl:text><xsl:value-of select="@fileName"/></xsl:with-param>
                    </xsl:call-template>
                    <xsl:apply-templates select="TechnicalMetadata[@key='Definition']"/>
                    <xsl:call-template name="ooyala-state"/>
                </value>
            </metadata-instance>
        </xsl:element>
    </xsl:template>

    <xsl:template name="rendition-placeholder">
        <xsl:param name="id"><xsl:value-of select="concat('CU_',@id,'_1.ts')"/></xsl:param>
        <xsl:param name="parent-id"><xsl:value-of select="@id"/></xsl:param>
        <xsl:element name="asset">
            <xsl:attribute name="type">rendition</xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="$id"/></xsl:attribute>
            <xsl:attribute name="parent-id"><xsl:value-of select="$parent-id"/></xsl:attribute>
            <metadata-instance name="ooyala-rendition">
                <value>
                    <xsl:call-template name="ooyala-state"/>
                </value>
            </metadata-instance>
        </xsl:element>
    </xsl:template>

    <xsl:template name="group-create-element">
        <xsl:param name="type"/>
        <xsl:param name="element_name"/>
        <xsl:param name="groupIds"/>
        <xsl:param name="addPrefix">1</xsl:param>

        <xsl:if test="string-length($type) = 0 or $type='series' or $type='season' or ($type='collection' and $element_name!='label') or
      ($type!='collection' and $element_name='label')">

            <xsl:if test="$element_name != 'season' and ((string-length($type) > 0 and  $type != $element_name) or string-length($type) = 0)">
                <xsl:variable name="id-prefix">
                    <xsl:choose>
                        <xsl:when test="$element_name='series' and $addPrefix='1'">s-</xsl:when>
                        <xsl:when test="$element_name='seriesseason' and $addPrefix='1'">s-</xsl:when>
                        <xsl:when test="$element_name='label' and $addPrefix='1'">l-</xsl:when>
                        <xsl:when test="$element_name='collection' and $addPrefix='1'">c-</xsl:when>
                    </xsl:choose>
                </xsl:variable>
                <xsl:element name="{$element_name}">
                    <xsl:attribute name="id"><xsl:value-of select="concat($id-prefix,$groupIds)"/></xsl:attribute>
                </xsl:element>

            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template name='assetGroups'>
        <xsl:param name='assetType'/>
        <xsl:param name="groupIds"/>
        <xsl:param name="groupType" />
        <xsl:param name="type" />
        <xsl:param name="addPrefix">1</xsl:param>
        <xsl:param name="includeOnly"/>

        <xsl:variable name="firstGroup">
            <xsl:choose>
                <xsl:when test="$groupType='channel' or $addPrefix='0'">
                    <xsl:value-of select="$groupIds"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring-before($groupIds, ' ')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="otherGroups">
            <xsl:choose>
                <xsl:when test="$groupType='channel' or $addPrefix='0'"/>
                <xsl:otherwise>
                    <xsl:value-of select="substring-after($groupIds, ' ')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <!-- Recurse if multiple groups (firstGroup not empty) -->
            <xsl:when test="string-length($firstGroup) > 0 and string-length($groupType) > 0">
                <xsl:variable name="groupType2">
                    <xsl:apply-templates select="/ScheduleProvider/Catalogue//Node[@id = $firstGroup]">
                        <xsl:with-param name="full">0</xsl:with-param>
                    </xsl:apply-templates>
                </xsl:variable>

                <xsl:variable name="element_name">
                    <xsl:choose>
                        <xsl:when test="string-length($groupType2) > 0">
                            <xsl:value-of select="$groupType2"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$groupType"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$element_name='collectionlabel' or $element_name='labelcollection' or $element_name='collectionlabelseries'">

                        <xsl:call-template name="group-create-element">
                            <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            <xsl:with-param name="element_name">collection</xsl:with-param>
                            <xsl:with-param name="groupIds"><xsl:value-of select="$firstGroup"/></xsl:with-param>
                            <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="group-create-element">
                            <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            <xsl:with-param name="element_name">label</xsl:with-param>
                            <xsl:with-param name="groupIds"><xsl:value-of select="$firstGroup"/></xsl:with-param>
                            <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:when test="$element_name='offer'">

                        <xsl:for-each select="key('productByID', $firstGroup)">
                            <xsl:if test="@type!='subscription'">
                                <xsl:call-template name="group-create-element">
                                    <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                    <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                                    <xsl:with-param name="groupIds"><xsl:value-of select="$firstGroup"/></xsl:with-param>
                                    <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="$type='series'">

                        <xsl:for-each select="key('catalogueByID', $firstGroup)">
                            <xsl:variable name="parent-id">
                                <xsl:value-of select="../../@id"/>
                            </xsl:variable>
                            <xsl:variable name="parent-title">
                                <xsl:value-of select="../../@title"/>
                            </xsl:variable>
                            <xsl:variable name="grandparent-title">
                                <xsl:value-of select="../../../@title"/>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$grandparent-title='InternetTV' and $parent-title='Go Select'">
                                    <xsl:call-template name="assetGroups">
                                        <xsl:with-param name="groupIds" select="$parent-id"/>
                                        <xsl:with-param name="groupType" select="$groupType"/>
                                        <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                        <xsl:with-param name="includeOnly">collection</xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:call-template name="group-create-element">
                                        <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                        <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                                        <xsl:with-param name="groupIds"><xsl:value-of select="$firstGroup"/></xsl:with-param>
                                        <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="group-create-element">
                                        <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                        <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                                        <xsl:with-param name="groupIds"><xsl:value-of select="$firstGroup"/></xsl:with-param>
                                        <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="group-create-element">
                            <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                            <xsl:with-param name="groupIds"><xsl:value-of select="$firstGroup"/></xsl:with-param>
                            <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:call-template name="assetGroups">
                    <xsl:with-param name="groupIds" select="$otherGroups"/>
                    <xsl:with-param name="groupType" select="$groupType"/>
                    <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <!-- Handle single group case (firstGroup empty, groupIds not empty) -->
            <xsl:when test="string-length($groupIds) > 0 and string-length($groupType) > 0">
                <xsl:variable name="groupType2">
                    <xsl:apply-templates select="/ScheduleProvider/Catalogue//Node[@id = $groupIds]">
                        <xsl:with-param name="full">0</xsl:with-param>
                    </xsl:apply-templates>
                </xsl:variable>

                <xsl:variable name="element_name">
                    <xsl:choose>
                        <xsl:when test="string-length($groupType2) > 0">
                            <xsl:value-of select="$groupType2"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$groupType"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="$element_name='collectionlabel' or $element_name='labelcollection' or $element_name='collectionlabelseries'">
                        <xsl:if test="string-length($includeOnly) = 0 or $includeOnly = 'collection'">
                            <xsl:call-template name="group-create-element">
                                <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                <xsl:with-param name="element_name">collection</xsl:with-param>
                                <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                                <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                        <xsl:if test="string-length($includeOnly) = 0 or $includeOnly = 'label'">
                            <xsl:call-template name="group-create-element">
                                <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                <xsl:with-param name="element_name">label</xsl:with-param>
                                <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                                <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="$element_name='seriesseason'">

                        <xsl:call-template name="group-create-element">
                            <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            <xsl:with-param name="element_name">series</xsl:with-param>
                            <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                            <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                        </xsl:call-template>


                    </xsl:when>
                    <xsl:when test="$element_name='offer'">
                        <xsl:for-each select="key('productByID', $groupIds)">
                            <xsl:if test="@type!='subscription'">
                                <xsl:call-template name="group-create-element">
                                    <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                    <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                                    <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                                    <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="$type='series'">
                        <xsl:for-each select="key('catalogueByID', $groupIds)">
                            <xsl:variable name="parent-id">
                                <xsl:value-of select="../@id"/>
                            </xsl:variable>
                            <xsl:variable name="parent-title">
                                <xsl:value-of select="../@title"/>
                            </xsl:variable>
                            <xsl:variable name="grandparent-title">
                                <xsl:value-of select="../../@title"/>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="$grandparent-title='InternetTV' and $parent-title='Go Select'">
                                    <xsl:call-template name="assetGroups">
                                        <xsl:with-param name="groupIds" select="$parent-id"/>
                                        <xsl:with-param name="groupType" select="$groupType"/>
                                        <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                        <xsl:with-param name="includeOnly">collection</xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:call-template name="group-create-element">
                                        <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                        <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                                        <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                                        <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="group-create-element">
                                        <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                                        <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                                        <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                                        <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="group-create-element">
                            <xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
                            <xsl:with-param name="element_name"><xsl:value-of select="$element_name"/></xsl:with-param>
                            <xsl:with-param name="groupIds"><xsl:value-of select="$groupIds"/></xsl:with-param>
                            <xsl:with-param name="addPrefix"><xsl:value-of select="$addPrefix"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name='get-parent-id'>
        <xsl:param name='assetType'/>
        <xsl:param name="parentIds"/>
        <xsl:param name="groupType" />
        <xsl:param name="moreParentIds" select="0"/>
        <xsl:variable name="firstGroup" select="substring-before($parentIds, ' ')"/>
        <xsl:variable name="otherGroups" select="substring-after($parentIds, ' ')"/>

        <xsl:choose>
            <!-- Recurse if multiple groups (firstGroup not empty) -->
            <xsl:when test="string-length($firstGroup) > 0 and string-length($groupType) > 0">
                <xsl:variable name="groupType2">
                    <xsl:apply-templates select="/ScheduleProvider/Catalogue//Node[@id = $firstGroup]">
                        <xsl:with-param name="full">0</xsl:with-param>
                    </xsl:apply-templates>
                </xsl:variable>

                <xsl:variable name="element_name">
                    <xsl:choose>
                        <xsl:when test="string-length($groupType2) > 0">
                            <xsl:value-of select="$groupType2"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$groupType"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:if test="$element_name = 'series' or $element_name = 'season' or $element_name = 'collectionlabelseries'">
                    <xsl:if test="$moreParentIds = 0"><xsl:value-of select="$firstGroup"/></xsl:if>
                    <xsl:if test="$moreParentIds = 1"><xsl:value-of select="concat(',','s-',$firstGroup)"/></xsl:if>
                </xsl:if>
                <xsl:call-template name="get-parent-id">
                    <xsl:with-param name="parentIds" select="$otherGroups"/>
                    <xsl:with-param name="groupType" select="$groupType"/>
                    <xsl:with-param name="moreParentIds" select="1"/>
                </xsl:call-template>

            </xsl:when>
            <!-- Handle single group case (firstGroup empty, parentIds not empty) -->
            <xsl:when test="string-length($parentIds) > 0">
                <xsl:variable name="groupType2">
                    <xsl:apply-templates select="/ScheduleProvider/Catalogue//Node[@id = $parentIds]">
                        <xsl:with-param name="full">0</xsl:with-param>
                    </xsl:apply-templates>
                </xsl:variable>

                <xsl:variable name="element_name">
                    <xsl:choose>
                        <xsl:when test="string-length($groupType2) > 0">
                            <xsl:value-of select="$groupType2"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$groupType"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:if test="$element_name = 'series' or $element_name = 'season' or $element_name = 'collectionlabelseries'">
                    <xsl:if test="$moreParentIds = 0"><xsl:value-of select="$parentIds"/></xsl:if>
                    <xsl:if test="$moreParentIds = 1"><xsl:value-of select="concat(',','s-',$parentIds)"/></xsl:if>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="get-special-parents">
        <xsl:param name="nodeId"/>
        <xsl:param name="ref">0</xsl:param>
        <xsl:for-each select="key('catalogueByID', $nodeId)">
            <xsl:variable name="parent-title">
                <xsl:value-of select="../@title"/>
            </xsl:variable>
            <xsl:variable name="grandparent-title">
                <xsl:value-of select="../../@title"/>
            </xsl:variable>
            <xsl:variable name="great-grandparent-title">
                <xsl:value-of select="../../../@title"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$parent-title = 'HBO On Demand' and
          (@title = 'Top Picks' or
          @title = 'Oscar' or
          @title = 'Movies' or
          @title = 'Specials' or
          @title = 'Family')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                        <!--xsl:otherwise>
                          <xsl:call-template name="catalogue-node">
                            <xsl:with-param name="type">label</xsl:with-param>
                            <xsl:with-param name="full"><xsl:value-of select="1"></xsl:value-of></xsl:with-param>
                            <xsl:with-param name="layer2-name"><xsl:value-of select="$grandparent-title"/></xsl:with-param>
                            <xsl:with-param name="node-id"><xsl:value-of select="../@id"/></xsl:with-param>
                            <xsl:with-param name="parent-id"><xsl:value-of select="../../@id"/></xsl:with-param>
                            <xsl:with-param name="title"><xsl:value-of select="../@title"/></xsl:with-param>
                            <xsl:with-param name="parent-name"><xsl:value-of select="../../@title"/></xsl:with-param>
                          </xsl:call-template>
                          <xsl:call-template name="catalogue-node">
                            <xsl:with-param name="type">collection</xsl:with-param>
                            <xsl:with-param name="full"><xsl:value-of select="1"></xsl:value-of></xsl:with-param>
                            <xsl:with-param name="layer2-name"><xsl:value-of select="$grandparent-title"/></xsl:with-param>
                            <xsl:with-param name="node-id"><xsl:value-of select="../@id"/></xsl:with-param>
                            <xsl:with-param name="parent-id"><xsl:value-of select="../../@id"/></xsl:with-param>
                            <xsl:with-param name="title"><xsl:value-of select="../@title"/></xsl:with-param>
                            <xsl:with-param name="parent-name"><xsl:value-of select="../../@title"/></xsl:with-param>
                          </xsl:call-template>
                        </xsl:otherwise-->
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$parent-title = 'Blockbuster Hangat 1' and
          (@title = 'New This Month' or
          @title = 'Movies')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$parent-title = 'CinemaWorld On Demand' or $parent-title = 'Dunia Sinema On Demand' or
          $parent-title = 'Eros Bollywood On Demand' or $parent-title = 'Now Baogu Movies' or
          $parent-title = 'Celestial Classic Movies on Demand' or $parent-title = 'Celestial Movies on Demand' or
          $parent-title = 'Rediffusion TV on Demand' or $parent-title = 'Varnam' or
          $parent-title = 'Disney Movies on Demand'">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$parent-title = 'Go Select' and
          (@title = 'Asian' or @title = 'Malay' or
          @title = 'Tamil'or @title='Kids' or
          @title='Factual &amp; Entertainment' or
          @title='Entertainment' or @title='Education' or
          @title='Catch Up' or @title='Lifestyle' or
          @title='HBO GO' or @title='TVB First')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$grandparent-title = 'On Demand Channels' and
          ($parent-title = 'Lifestyle On Demand' or $parent-title = 'Entertainment On Demand' or
          $parent-title = 'Kids On Demand'or $parent-title='Education On Demand' or
          $parent-title='WWE Series On Demand')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$grandparent-title = 'HBO On Demand' and
          ($parent-title = 'Top Picks' or
          $parent-title = 'Oscar' or
          $parent-title = 'Movies' or
          $parent-title = 'Specials' or
          $parent-title = 'Family')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                        <!--xsl:otherwise>
                          <xsl:call-template name="catalogue-node">
                            <xsl:with-param name="type">collection</xsl:with-param>
                            <xsl:with-param name="full"><xsl:value-of select="1"></xsl:value-of></xsl:with-param>
                            <xsl:with-param name="layer2-name"><xsl:value-of select="$great-grandparent-title"/></xsl:with-param>
                            <xsl:with-param name="node-id"><xsl:value-of select="../../@id"/></xsl:with-param>
                            <xsl:with-param name="parent-id"><xsl:value-of select="../../../@id"/></xsl:with-param>
                            <xsl:with-param name="title"><xsl:value-of select="../../@title"/></xsl:with-param>
                            <xsl:with-param name="parent-name"><xsl:value-of select="../../../@title"/></xsl:with-param>
                          </xsl:call-template>
                          <xsl:call-template name="catalogue-node">
                            <xsl:with-param name="type">label</xsl:with-param>
                            <xsl:with-param name="full"><xsl:value-of select="1"></xsl:value-of></xsl:with-param>
                            <xsl:with-param name="layer2-name"><xsl:value-of select="$great-grandparent-title"/></xsl:with-param>
                            <xsl:with-param name="node-id"><xsl:value-of select="../../@id"/></xsl:with-param>
                            <xsl:with-param name="parent-id"><xsl:value-of select="../../../@id"/></xsl:with-param>
                            <xsl:with-param name="title"><xsl:value-of select="../../@title"/></xsl:with-param>
                            <xsl:with-param name="parent-name"><xsl:value-of select="../../../@title"/></xsl:with-param>
                          </xsl:call-template>
                        </xsl:otherwise-->
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$grandparent-title = 'Blockbuster Hangat 1' and
          ($parent-title = 'New This Month' or
          $parent-title = 'Movies')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$grandparent-title = 'Ruyi Hokkien Channel On Demand' and
          (contains($parent-title,'New This Month') or
          $parent-title = 'CNY Specials')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$grandparent-title = 'CinemaWorld On Demand' or $grandparent-title = 'Dunia Sinema On Demand' or
          $grandparent-title = 'Eros Bollywood On Demand' or $grandparent-title = 'Now Baogu Movies' or
          $grandparent-title = 'Celestial Classic Movies on Demand' or $grandparent-title = 'Celestial Movies on Demand' or
          $grandparent-title = 'Rediffusion TV on Demand' or $grandparent-title = 'Varnam' or
          $grandparent-title = 'Disney Movies on Demand'">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$grandparent-title = 'Go Select' and
          ($parent-title = 'Asian' or $parent-title = 'Malay' or
          $parent-title = 'Tamil'or $parent-title='Kids' or
          $parent-title='Factual &amp; Entertainment' or
          $parent-title='Entertainment' or $parent-title='Education' or
          $parent-title='Catch Up' or $parent-title='Lifestyle' or
          $parent-title='HBO GO' or $parent-title='TVB First')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$great-grandparent-title = 'Go Select' and
          ($grandparent-title = 'Asian' or $grandparent-title = 'Malay' or
          $grandparent-title = 'Tamil'or $grandparent-title='Kids' or
          $grandparent-title='Factual &amp; Entertainment' or
          $grandparent-title='Entertainment' or $grandparent-title='Education' or
          $grandparent-title='Catch Up' or $grandparent-title='Lifestyle' or
          $grandparent-title='HBO GO' or $grandparent-title='TVB First')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="$great-grandparent-title = 'On Demand Channels' and
          ($grandparent-title = 'Lifestyle On Demand' or $grandparent-title = 'Entertainment On Demand' or
          $grandparent-title = 'Kids On Demand'or $grandparent-title='Education On Demand' or
          $grandparent-title='WWE Series On Demand')">
                    <xsl:choose>
                        <xsl:when test="$ref=1">
                            <xsl:element name="label">
                                <xsl:attribute name="id"><xsl:value-of select="concat('l-',../@id)"/></xsl:attribute>
                            </xsl:element>
                            <xsl:element name="collection">
                                <xsl:attribute name="id"><xsl:value-of select="concat('c-',../@id)"/></xsl:attribute>
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                </xsl:when>
                <xsl:when test="ancestor::Node/@title='hiddennode'">
                    <xsl:comment><xsl:text>delete-me-please</xsl:text></xsl:comment>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="special-handling">
        <xsl:param name="list" select="@nodeRefs"/>
        <xsl:param name="delimiter">
            <xsl:text> </xsl:text>
        </xsl:param>
        <xsl:param name="ref">0</xsl:param>
        <xsl:choose>
            <xsl:when test="contains($list, $delimiter)">
                <!-- get everything in front of the first delimiter -->
                <xsl:call-template name="get-special-parents">
                    <xsl:with-param name="nodeId" select="substring-before($list,$delimiter)"/>
                    <xsl:with-param name="ref"><xsl:value-of select="$ref"/></xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="special-handling">
                    <!-- store anything left in another variable -->
                    <xsl:with-param name="list" select="substring-after($list,$delimiter)"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                    <xsl:with-param name="ref"><xsl:value-of select="$ref"/></xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$list = ''">
                        <xsl:text/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="get-special-parents">
                            <xsl:with-param name="nodeId" select="$list"/>
                            <xsl:with-param name="ref"><xsl:value-of select="$ref"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name='asset'>
        <xsl:param name='assetType'/>
        <xsl:param name='assetId' select='@id'/>
        <xsl:param name='parentId'/>
        <xsl:param name='collections'/>
        <xsl:param name='labels'/>
        <xsl:param name='contentId' select='@contentRef'/>
        <xsl:param name='channelId'/>
        <xsl:param name='isEPG'>0</xsl:param>

        <!--xsl:if test="$assetType='episode' or $assetType='movie'">
          <xsl:call-template name="special-handling"/>
        </xsl:if-->

        <!--Skip asset creation if Content/Media/TechnicalMetadata[@key='Status'] is not 'Processed'-->
        <xsl:variable name="contentStatus" select="/ScheduleProvider/Content[@id = $contentId]/Media/TechnicalMetadata[@key='Status']"/>
        <xsl:if test="not($contentStatus) or ($contentStatus and $contentStatus = 'Processed')">

            <xsl:element name="asset">
                <xsl:attribute name="type"><xsl:value-of select="$assetType"/></xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="$assetId"/></xsl:attribute>
                <xsl:if test="$assetType='episode' and $parentId">
                    <xsl:variable name="parents">
                        <xsl:call-template name="get-parent-id">
                            <xsl:with-param name="parentIds" select="$parentId"/>
                            <xsl:with-param name="groupType">series</xsl:with-param>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:if test="string-length($parents) > 0">
                        <xsl:attribute name="parent-id">
                            <xsl:value-of select="$parents"/>
                        </xsl:attribute>
                    </xsl:if>
                    <!--xsl:attribute name="parent-id"><xsl:value-of select="$parentId"/></xsl:attribute-->
                </xsl:if>
                <xsl:variable name="firstNodeRef">
                    <xsl:choose>
                        <xsl:when test="contains(@nodeRefs, ' ')">
                            <xsl:value-of select="substring-before(@nodeRefs, ' ')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@nodeRefs"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <groups>
                    <xsl:call-template name="assetGroups">
                        <xsl:with-param name="groupIds" select="@productRefs"/>
                        <xsl:with-param name="groupType">offer</xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="assetGroups">
                        <xsl:with-param name="groupIds" select="$collections"/>
                        <xsl:with-param name="groupType">collection</xsl:with-param>
                    </xsl:call-template>

                    <xsl:if test="$assetType='episode' or $assetType='movie'">
                        <xsl:call-template name="special-handling">
                            <xsl:with-param name="ref">1</xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>

                    <xsl:call-template name="assetGroups">
                        <xsl:with-param name="groupIds" select="$channelId"/>
                        <xsl:with-param name="groupType">channel</xsl:with-param>
                    </xsl:call-template>

                    <xsl:if test='$isEPG=1 and $assetType!="channel" and @isCatchUp="1"'>
                        <xsl:call-template name="assetGroups">
                            <xsl:with-param name="groupIds" select="concat('catchup_',$channelId)"/>
                            <xsl:with-param name="groupType">collection</xsl:with-param>
                            <xsl:with-param name="addPrefix">0</xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>

                </groups>
                <xsl:element name="metadata-instance">

                    <xsl:attribute name="name">
                        <xsl:choose>
                            <xsl:when test="$assetType='channel'">ooyala-live-</xsl:when>
                            <xsl:otherwise>perfstarhub-</xsl:otherwise>
                        </xsl:choose><xsl:value-of select="$assetType"/>
                    </xsl:attribute>
                    <value>
                        <xsl:if test='$isEPG!=1'>
                            <xsl:call-template name="lysis-id"/>
                            <xsl:call-template name='string'>
                                <xsl:with-param name="name">display-name</xsl:with-param>
                                <xsl:with-param name="value">
                                    <xsl:for-each select="key('contentByID', @contentRef)">
                                        <xsl:value-of select="@title"/>
                                    </xsl:for-each>
                                </xsl:with-param>
                            </xsl:call-template>
                            <xsl:call-template name='string'>
                                <xsl:with-param name="name">title-internal</xsl:with-param>
                                <xsl:with-param name="value">
                                    <xsl:for-each select="key('contentByID', @contentRef)">
                                        <xsl:value-of select="@title"/>
                                    </xsl:for-each>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>

                        <xsl:if test='$isEPG!=1'>
                            <xsl:call-template name='string'>
                                <xsl:with-param name="name">lysis-layer2-node-cat</xsl:with-param>
                                <xsl:with-param name="value">
                                    <xsl:for-each select="key('catalogueByID', $firstNodeRef)">
                                        <xsl:variable name="parent-title">
                                            <xsl:value-of select="../@title"/>
                                        </xsl:variable>
                                        <xsl:variable name="grandparent-title">
                                            <xsl:value-of select="../../@title"/>
                                        </xsl:variable>
                                        <xsl:choose>
                                            <xsl:when test="$parent-title = 'Movies' or $parent-title = 'TV Series' or
                       $parent-title = 'On Demand Channels' or $parent-title = 'InternetTV'">
                                                <xsl:value-of select="$parent-title"/>
                                            </xsl:when>
                                            <xsl:when test="$grandparent-title = 'Movies' or $grandparent-title = 'TV Series' or
                       $grandparent-title = 'On Demand Channels' or $grandparent-title = 'InternetTV'">
                                                <xsl:value-of select="$grandparent-title"/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:for-each>
                                </xsl:with-param>
                            </xsl:call-template>
                            <xsl:call-template name="Licence">
                                <xsl:with-param name="license-start-time">
                                    <xsl:for-each select="key('contentByID', @contentRef)">
                                        <xsl:value-of select="Licence/@saleValidityStart"/>
                                    </xsl:for-each>
                                </xsl:with-param>
                                <xsl:with-param name="license-end-time">
                                    <xsl:for-each select="key('contentByID', @contentRef)">
                                        <xsl:value-of select="Licence/@saleValidityEnd"/>
                                    </xsl:for-each>
                                </xsl:with-param>
                            </xsl:call-template>
                            <xsl:call-template name="OTT"/>
                            <string-instance name="ooyala-state">
                                <value>published</value>
                            </string-instance>
                        </xsl:if>
                        <xsl:if test='$isEPG=1 and $assetType="channel"'>
                            <xsl:call-template name='string'>
                                <xsl:with-param name="name">name</xsl:with-param>
                                <xsl:with-param name="value"><xsl:value-of select="../Service/@longName"/></xsl:with-param>
                            </xsl:call-template>
                            <xsl:call-template name="channel-data" />
                        </xsl:if>
                        <xsl:if test='$isEPG=1'>
                            <xsl:if test='$assetType!="channel"'>
                                <xsl:call-template name='string'>
                                    <xsl:with-param name="name">display-name</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="@title"/></xsl:with-param>
                                </xsl:call-template>
                                <xsl:call-template name='string'>
                                    <xsl:with-param name="name">title-internal</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="@title"/></xsl:with-param>
                                </xsl:call-template>
                                <xsl:variable name="realChannelId">
                                    <xsl:call-template name="channel-id-mapping">
                                        <xsl:with-param name="service-id">
                                            <xsl:value-of select="../../Service/@id"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:call-template name='string'>
                                    <xsl:with-param name="name">epg-channel</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="$realChannelId"/></xsl:with-param>
                                </xsl:call-template>
                                <xsl:call-template name='string'>
                                    <xsl:with-param name="name">lysis-id</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="@id"/></xsl:with-param>
                                </xsl:call-template>
                                <xsl:call-template name='boolean'>
                                    <xsl:with-param name="name">is-blackout</xsl:with-param>
                                    <xsl:with-param name="value">
                                        <xsl:choose>
                                            <xsl:when test="EpgDescription/EpgElement[@key='BlackedOutInternet'] = 'true'">true</xsl:when>
                                            <xsl:otherwise>false</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:with-param>
                                </xsl:call-template>
                                <xsl:call-template name='boolean'>
                                    <xsl:with-param name="name">is-catchup</xsl:with-param>
                                    <xsl:with-param name="value">
                                        <xsl:choose>
                                            <xsl:when test="@isCatchUp = '1' and EpgDescription/EpgElement[@key='CatchUpInternet'] = 'true'">true</xsl:when>
                                            <xsl:otherwise>false</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:with-param>
                                </xsl:call-template>
                                <xsl:call-template name='string'>
                                    <xsl:with-param name="name">provider</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="../../Service/@longName"/></xsl:with-param>
                                </xsl:call-template>
                                <xsl:if test="@isCatchUp = '1' and EpgDescription/EpgElement[@key='CatchUpInternet'] = 'true'">
                                    <xsl:call-template name="OTT">
                                        <xsl:with-param name="start"><xsl:value-of select="Period/@start"/></xsl:with-param>
                                        <xsl:with-param name="end">
                                            <xsl:call-template name="calcDate">
                                                <xsl:with-param name="date" select="Period/@start"/>
                                                <xsl:with-param name="days">30</xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:if>
                                <xsl:call-template name='time'>
                                    <xsl:with-param name="name">live-start-time</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="Period/@start"/></xsl:with-param>
                                </xsl:call-template>
                                <xsl:call-template name='time'>
                                    <xsl:with-param name="name">live-end-time</xsl:with-param>
                                    <xsl:with-param name="value"><xsl:value-of select="Period/@end"/></xsl:with-param>
                                </xsl:call-template>
                                <string-instance name="ooyala-state">
                                    <value>published</value>
                                </string-instance>
                            </xsl:if>
                        </xsl:if>

                        <xsl:apply-templates select="EpgDescription">
                            <xsl:with-param name="isEPG">
                                <xsl:value-of select="$isEPG"/>
                            </xsl:with-param>
                        </xsl:apply-templates>
                    </value>
                </xsl:element>
            </xsl:element>

            <xsl:call-template name="image">
                <xsl:with-param name="assetId" select="@id"/>
            </xsl:call-template>

            <xsl:if test='$contentId'>
                <xsl:apply-templates select="/ScheduleProvider/Content[@id = $contentId]/Media">
                    <xsl:with-param name="assetId" select="@id"/>
                </xsl:apply-templates>
            </xsl:if>

            <xsl:if test='$isEPG=1 and $assetType!="channel" and @isCatchUp="1" and EpgDescription/EpgElement[@key="CatchUpInternet"]="true"'>
                <xsl:call-template name="rendition-placeholder"/>
            </xsl:if>

        </xsl:if>
    </xsl:template>

    <xsl:template match="VodItem">
        <xsl:choose>
            <xsl:when test="(EpgDescription/EpgElement[@key='EpisodeNumber'] and floor(EpgDescription/EpgElement[@key='EpisodeNumber']) = EpgDescription/EpgElement[@key='EpisodeNumber']) or substring(EpgDescription/EpgElement[@key='EpisodeNumber'],1,2) = 'Ep'">
                <xsl:call-template name='asset'>
                    <xsl:with-param name='assetType'>episode</xsl:with-param>
                    <xsl:with-param name='parentId' select='@nodeRefs'/>
                    <xsl:with-param name='collections' select='@nodeRefs'/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name='asset'>
                    <xsl:with-param name='assetType'>movie</xsl:with-param>
                    <xsl:with-param name='collections' select='@nodeRefs'/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!--
    perfstarhub EPG
    -->
    <xsl:template match="DownloadPeriod">
        <xsl:variable name="realChannelId">
            <xsl:call-template name="channel-id-mapping">
                <xsl:with-param name="service-id">
                    <xsl:value-of select="@serviceRef"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:call-template name='asset'>
            <xsl:with-param name='assetType'>channel</xsl:with-param>
            <xsl:with-param name='assetId'><xsl:value-of select="$realChannelId"/></xsl:with-param>
            <xsl:with-param name='isEPG'>1</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="catalogue-node">
            <xsl:with-param name="type">collection</xsl:with-param>
            <xsl:with-param name="full"><xsl:value-of select="1"/></xsl:with-param>
            <xsl:with-param name="layer2-name">catchup</xsl:with-param>
            <xsl:with-param name='title'><xsl:value-of select="$realChannelId"/></xsl:with-param>
            <xsl:with-param name='node-id'><xsl:value-of select="concat('catchup_',$realChannelId)"/></xsl:with-param>
            <xsl:with-param name="addPrefix">0</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="Programme">
            <xsl:with-param name='channelId'><xsl:value-of select="$realChannelId"/></xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template name="channel-id-mapping">
        <xsl:param name="service-id"/>

        <xsl:choose>
            <xsl:when test='$service-id=624'><xsl:text>[815]_KBS World HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=279'><xsl:text>[122]_MNC Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=270'><xsl:text>[123]_Sensasi</xsl:text></xsl:when>
            <xsl:when test='$service-id=141'><xsl:text>[125]_ZeeTV</xsl:text></xsl:when>
            <xsl:when test='$service-id=225'><xsl:text>[127]_Sony Entertainment</xsl:text></xsl:when>
            <xsl:when test='$service-id=325'><xsl:text>[130]_Zee Cinema</xsl:text></xsl:when>
            <xsl:when test='$service-id=308'><xsl:text>[136]_Vannathirai</xsl:text></xsl:when>
            <xsl:when test='$service-id=657'><xsl:text>[137]_V Thamizh</xsl:text></xsl:when>
            <xsl:when test='$service-id=282'><xsl:text>[144]_The Filipino Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=617'><xsl:text>[145]_Cinema One Global</xsl:text></xsl:when>
            <xsl:when test='$service-id=35'><xsl:text>[152]_TV5Monde</xsl:text></xsl:when>
            <xsl:when test='$service-id=661'><xsl:text>[157]_Zee TV HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=660'><xsl:text>[161]_NDTV Good Times</xsl:text></xsl:when>
            <xsl:when test='$service-id=659'><xsl:text>[162]_NDTV 24x7</xsl:text></xsl:when>
            <xsl:when test='$service-id=163'><xsl:text>[163]_SAB TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=165'><xsl:text>[165]_&amp;TV HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=931'><xsl:text>[170]_MNC Info</xsl:text></xsl:when>
            <xsl:when test='$service-id=348'><xsl:text>[201]_Super Sports1 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=23'><xsl:text>[202]_Super Sports 2 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=22'><xsl:text>[203]_Super Sports3 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=54'><xsl:text>[204]_SuperSports 4 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=112'><xsl:text>[205]_Super Sports Arena HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=628'><xsl:text>[208]_Fox Sports HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=629'><xsl:text>[209]_Fox Sports 2 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=630'><xsl:text>[210]_Fox Sports 3 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=296'><xsl:text>[212]_Eurosport HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=319'><xsl:text>[215]_ASN</xsl:text></xsl:when>
            <xsl:when test='$service-id=390'><xsl:text>[217]_Setanta Sports</xsl:text></xsl:when>
            <xsl:when test='$service-id=655'><xsl:text>[225]_Red Card Sports Radio</xsl:text></xsl:when>
            <xsl:when test='$service-id=347'><xsl:text>[235]_Ten Cricket</xsl:text></xsl:when>
            <xsl:when test='$service-id=632'><xsl:text>[236]_StarCricket HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=947'><xsl:text>[237]_StarCricket Extra HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=938'><xsl:text>[301]_BabyFirst</xsl:text></xsl:when>
            <xsl:when test='$service-id=327'><xsl:text>[303]_Cbeebies</xsl:text></xsl:when>
            <xsl:when test='$service-id=384'><xsl:text>[304]_Nick JR</xsl:text></xsl:when>
            <xsl:when test='$service-id=925'><xsl:text>[306]_ZooMoo HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=614'><xsl:text>[308]_Discovery Kids Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=637'><xsl:text>[310]_Disney XD HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=135'><xsl:text>[311]_Disney Junior</xsl:text></xsl:when>
            <xsl:when test='$service-id=24'><xsl:text>[312]_Disney Channel HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=943'><xsl:text>[314]_Nickelodeon</xsl:text></xsl:when>
            <xsl:when test='$service-id=251'><xsl:text>[315]_Toonami</xsl:text></xsl:when>
            <xsl:when test='$service-id=11'><xsl:text>[316]_Cartoon Network</xsl:text></xsl:when>
            <xsl:when test='$service-id=388'><xsl:text>[322]_MaxToon HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=276'><xsl:text>[401]_History HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=646'><xsl:text>[402]_H2 HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=651'><xsl:text>[403]_Crime &amp; Investigation Network (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=650'><xsl:text>[404]_FYI HD Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=326'><xsl:text>[407]_BBC Earth</xsl:text></xsl:when>
            <xsl:when test='$service-id=6'><xsl:text>[411]_National Geographic Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=373'><xsl:text>[412]_Nat Geo People</xsl:text></xsl:when>
            <xsl:when test='$service-id=4'><xsl:text>[422]_Discovery Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=126'><xsl:text>[423]_Discovery Science</xsl:text></xsl:when>
            <xsl:when test='$service-id=5'><xsl:text>[424]_Animal Planet</xsl:text></xsl:when>
            <xsl:when test='$service-id=125'><xsl:text>[425]_EVE</xsl:text></xsl:when>
            <xsl:when test='$service-id=127'><xsl:text>[426]_DMAX</xsl:text></xsl:when>
            <xsl:when test='$service-id=52'><xsl:text>[427]_TLC</xsl:text></xsl:when>
            <xsl:when test='$service-id=428'><xsl:text>[428]_RTL CBS Extreme HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=322'><xsl:text>[432]_BBC Lifestyle</xsl:text></xsl:when>
            <xsl:when test='$service-id=358'><xsl:text>[433]_Food Network</xsl:text></xsl:when>
            <xsl:when test='$service-id=238'><xsl:text>[435]_AFC</xsl:text></xsl:when>
            <xsl:when test='$service-id=437'><xsl:text>[437]_HGTV HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=131'><xsl:text>[441]_E! Entertainment</xsl:text></xsl:when>
            <xsl:when test='$service-id=442'><xsl:text>[442]_Fashion One</xsl:text></xsl:when>
            <xsl:when test='$service-id=253'><xsl:text>[443]_Fashion TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=246'><xsl:text>[461]_Discovery HD World</xsl:text></xsl:when>
            <xsl:when test='$service-id=612'><xsl:text>[473]_Travel Channel HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=509'><xsl:text>[509]_RTL CBS Entertainment HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=510'><xsl:text>[510]_Sony Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=355'><xsl:text>[511]_AXN HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=382'><xsl:text>[512]_Universal Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=923'><xsl:text>[513]_DIVA HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=648'><xsl:text>[514]_Lifetime HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=653'><xsl:text>[515]_Warner TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=620'><xsl:text>[516]_Comedy Central Asia HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=381'><xsl:text>[517]_Syfy</xsl:text></xsl:when>
            <xsl:when test='$service-id=922'><xsl:text>[519]_HITS HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=239'><xsl:text>[521]_BBC Entertainment</xsl:text></xsl:when>
            <xsl:when test='$service-id=529'><xsl:text>[529]_Aniplus HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=399'><xsl:text>[538]_iConcert</xsl:text></xsl:when>
            <xsl:when test='$service-id=605'><xsl:text>[613]_CinemaWorld HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=621'><xsl:text>[701]_BBC World News HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=2'><xsl:text>[707]_CNBC</xsl:text></xsl:when>
            <xsl:when test='$service-id=622'><xsl:text>[711]_CNN HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=264'><xsl:text>[712]_CNN Headline News</xsl:text></xsl:when>
            <xsl:when test='$service-id=27'><xsl:text>[801]_CCTV-4</xsl:text></xsl:when>
            <xsl:when test='$service-id=290'><xsl:text>[808]_TVBS News</xsl:text></xsl:when>
            <xsl:when test='$service-id=623'><xsl:text>[809]_China Business News (CBN)</xsl:text></xsl:when>
            <xsl:when test='$service-id=816'><xsl:text>[816]_Oh!K</xsl:text></xsl:when>
            <xsl:when test='$service-id=380'><xsl:text>[823]_One HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=34'><xsl:text>[825]_E City</xsl:text></xsl:when>
            <xsl:when test='$service-id=17'><xsl:text>[827]_CTI</xsl:text></xsl:when>
            <xsl:when test='$service-id=832'><xsl:text>[832]_Dragon TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=343'><xsl:text>[833]_Asia Travel</xsl:text></xsl:when>
            <xsl:when test='$service-id=29'><xsl:text>[838]_TVBJ</xsl:text></xsl:when>
            <xsl:when test='$service-id=395'><xsl:text>[846]_KMTV</xsl:text></xsl:when>
            <xsl:when test='$service-id=851'><xsl:text>[851]_now Hairunomneon</xsl:text></xsl:when>
            <xsl:when test='$service-id=38'><xsl:text>[855]_VVD HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=611'><xsl:text>[9001]_GINX</xsl:text></xsl:when>
            <xsl:when test='$service-id=200'><xsl:text>[102]_MediaCorp Ch 5</xsl:text></xsl:when>
            <xsl:when test='$service-id=201'><xsl:text>[103]_MediaCorp Ch 8</xsl:text></xsl:when>
            <xsl:when test='$service-id=202'><xsl:text>[106]_MediaCorp CNA</xsl:text></xsl:when>
            <xsl:when test='$service-id=203'><xsl:text>[104]_MediaCorp Suria</xsl:text></xsl:when>
            <xsl:when test='$service-id=204'><xsl:text>[105]_Vasantham</xsl:text></xsl:when>
            <xsl:when test='$service-id=206'><xsl:text>[107]_Channel U</xsl:text></xsl:when>
            <xsl:when test='$service-id=301'><xsl:text>[155]_HD 5</xsl:text></xsl:when>
            <xsl:when test='$service-id=313'><xsl:text>[108]_okto</xsl:text></xsl:when>
            <xsl:when test='$service-id=14'><xsl:text>[828]_TVBS Asia</xsl:text></xsl:when>
            <xsl:when test='$service-id=18'><xsl:text>[533]_MTV S.E.A</xsl:text></xsl:when>
            <xsl:when test='$service-id=25'><xsl:text>[133]_SUN TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=30'><xsl:text>[126]_STAR Plus</xsl:text></xsl:when>
            <xsl:when test='$service-id=36'><xsl:text>[153]_Deutsche Welle</xsl:text></xsl:when>
            <xsl:when test='$service-id=38'><xsl:text>[855]_VV Drama</xsl:text></xsl:when>
            <xsl:when test='$service-id=39'><xsl:text>[811]_NHK World Premium</xsl:text></xsl:when>
            <xsl:when test='$service-id=44'><xsl:text>[829]_TVB 8</xsl:text></xsl:when>
            <xsl:when test='$service-id=47'><xsl:text>[708]_Bloomberg Television</xsl:text></xsl:when>
            <xsl:when test='$service-id=56'><xsl:text>[868]_Celestial Movies</xsl:text></xsl:when>
            <xsl:when test='$service-id=112'><xsl:text>[112]_SuperSports Arena</xsl:text></xsl:when>
            <xsl:when test='$service-id=118'><xsl:text>[118]_Astro Warna</xsl:text></xsl:when>
            <xsl:when test='$service-id=140'><xsl:text>[139]_Asianet</xsl:text></xsl:when>
            <xsl:when test='$service-id=143'><xsl:text>[722]_CCTV News</xsl:text></xsl:when>
            <xsl:when test='$service-id=150'><xsl:text>[616]_Turner Classic Movies</xsl:text></xsl:when>
            <xsl:when test='$service-id=221'><xsl:text>[821]_XING KONG</xsl:text></xsl:when>
            <xsl:when test='$service-id=228'><xsl:text>[532]_Animax</xsl:text></xsl:when>
            <xsl:when test='$service-id=233'><xsl:text>[288]_Horse Racing Ch88</xsl:text></xsl:when>
            <xsl:when test='$service-id=234'><xsl:text>[289]_Horse Racing Ch89</xsl:text></xsl:when>
            <xsl:when test='$service-id=241'><xsl:text>[702]_FOX News Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=245'><xsl:text>[457]_National Geographic Channel HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=250'><xsl:text>[822]_STAR Chinese Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=252'><xsl:text>[531]_Channel [V] International</xsl:text></xsl:when>
            <xsl:when test='$service-id=259'><xsl:text>[135]_VIJAY</xsl:text></xsl:when>
            <xsl:when test='$service-id=261'><xsl:text>[848]_[V] China</xsl:text></xsl:when>
            <xsl:when test='$service-id=262'><xsl:text>[847]_[V] Taiwan</xsl:text></xsl:when>
            <xsl:when test='$service-id=273'><xsl:text>[129]_STAR Gold</xsl:text></xsl:when>
            <xsl:when test='$service-id=277'><xsl:text>[302]_BabyTV</xsl:text></xsl:when>
            <xsl:when test='$service-id=278'><xsl:text>[867]_STAR Chinese Movies Legend</xsl:text></xsl:when>
            <xsl:when test='$service-id=281'><xsl:text>[211]_Eurosportnews</xsl:text></xsl:when>
            <xsl:when test='$service-id=283'><xsl:text>[817]_Arirang TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=286'><xsl:text>[447]_Australia Plus Television</xsl:text></xsl:when>
            <xsl:when test='$service-id=288'><xsl:text>[859]_TVB Xing He</xsl:text></xsl:when>
            <xsl:when test='$service-id=289'><xsl:text>[858]_TVB Classic</xsl:text></xsl:when>
            <xsl:when test='$service-id=317'><xsl:text>[317]_Boomerang HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=318'><xsl:text>[869]_Celestial Classic Movies</xsl:text></xsl:when>
            <xsl:when test='$service-id=333'><xsl:text>[601]_HBO HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=335'><xsl:text>[151]_Russia Today</xsl:text></xsl:when>
            <xsl:when test='$service-id=338'><xsl:text>[446]_truTV</xsl:text></xsl:when>
            <xsl:when test='$service-id=341'><xsl:text>[535]_Nat Geo Music</xsl:text></xsl:when>
            <xsl:when test='$service-id=345'><xsl:text>[518]_KIX</xsl:text></xsl:when>
            <xsl:when test='$service-id=346'><xsl:text>[618]_Thrill</xsl:text></xsl:when>
            <xsl:when test='$service-id=349'><xsl:text>[140]_Asianet Movies</xsl:text></xsl:when>
            <xsl:when test='$service-id=350'><xsl:text>[216]_NBA TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=357'><xsl:text>[413]_Nat Geo Wild</xsl:text></xsl:when>
            <xsl:when test='$service-id=359'><xsl:text>[501]_STAR World</xsl:text></xsl:when>
            <xsl:when test='$service-id=360'><xsl:text>[507]_FX HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=361'><xsl:text>[505]_FOX HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=370'><xsl:text>[503]_FOXCRIME</xsl:text></xsl:when>
            <xsl:when test='$service-id=372'><xsl:text>[622]_FOX Movies Premium HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=374'><xsl:text>[812]_NHK World TV</xsl:text></xsl:when>
            <xsl:when test='$service-id=376'><xsl:text>[128]_COLORS</xsl:text></xsl:when>
            <xsl:when test='$service-id=377'><xsl:text>[807]_Shen Zhou News Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=379'><xsl:text>[124]_ONE MALAY</xsl:text></xsl:when>
            <xsl:when test='$service-id=380'><xsl:text>[820]_ONE (English)</xsl:text></xsl:when>
            <xsl:when test='$service-id=387'><xsl:text>[605]_HBO Hits</xsl:text></xsl:when>
            <xsl:when test='$service-id=389'><xsl:text>[121]_IDX</xsl:text></xsl:when>
            <xsl:when test='$service-id=396'><xsl:text>[866]_STAR Chinese Movies</xsl:text></xsl:when>
            <xsl:when test='$service-id=600'><xsl:text>[624]_FOX Family Movies HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=601'><xsl:text>[824]_Channel [V] Mainland China</xsl:text></xsl:when>
            <xsl:when test='$service-id=602'><xsl:text>[703]_SKY NEWS HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=603'><xsl:text>[132]_Channel [V] India</xsl:text></xsl:when>
            <xsl:when test='$service-id=610'><xsl:text>[134]_Sun Music</xsl:text></xsl:when>
            <xsl:when test='$service-id=616'><xsl:text>[143]_ANC</xsl:text></xsl:when>
            <xsl:when test='$service-id=618'><xsl:text>[131]_SONY Max</xsl:text></xsl:when>
            <xsl:when test='$service-id=625'><xsl:text>[625]_FOX Action Movies HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=627'><xsl:text>[207]_FOX SPORTS NEWS</xsl:text></xsl:when>
            <xsl:when test='$service-id=633'><xsl:text>[603]_HBO Signature</xsl:text></xsl:when>
            <xsl:when test='$service-id=634'><xsl:text>[604]_HBO Family</xsl:text></xsl:when>
            <xsl:when test='$service-id=639'><xsl:text>[158]_Zee News</xsl:text></xsl:when>
            <xsl:when test='$service-id=640'><xsl:text>[861]_now Baogu Movies</xsl:text></xsl:when>
            <xsl:when test='$service-id=656'><xsl:text>[138]_Zee Tamizh</xsl:text></xsl:when>
            <xsl:when test='$service-id=658'><xsl:text>[160]_Life OK</xsl:text></xsl:when>
            <xsl:when test='$service-id=813'><xsl:text>[813]_WakuWaku Japan</xsl:text></xsl:when>
            <xsl:when test='$service-id=910'><xsl:text>[111]_E City</xsl:text></xsl:when>
            <xsl:when test='$service-id=924'><xsl:text>[611]_Cinemax HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=926'><xsl:text>[805]_Phoenix Chinese Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=927'><xsl:text>[806]_Phoenix InfoNews Channel</xsl:text></xsl:when>
            <xsl:when test='$service-id=1301'><xsl:text>[226]_mioStadium Multiview 101</xsl:text></xsl:when>
            <xsl:when test='$service-id=1302'><xsl:text>[227]_mioStadium 102 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=1303'><xsl:text>[228]_mioStadium 103 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=1304'><xsl:text>[229]_mioStadium 104 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=1305'><xsl:text>[230]_mioStadium 105 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=1306'><xsl:text>[231]_mioStadium 106 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=1307'><xsl:text>[232]_mioStadium 107 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=1308'><xsl:text>[233]_mioStadium 107 (HD)</xsl:text></xsl:when>
            <xsl:when test='$service-id=213'><xsl:text>[213]_BeIN Sports HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=218'><xsl:text>[218]_Sports PPV HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=220'><xsl:text>[220]_Eleven Euro HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=222'><xsl:text>[221]_Eleven Sports Euro HD</xsl:text></xsl:when>
            <xsl:when test='$service-id=8888'><xsl:text>[8888]_OTT Ad Hoc Channel 1</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="$service-id"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="channel-data">
        <complex-instance name="custom-metadata">
            <value>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-key</xsl:with-param>
                    <xsl:with-param name="value">long-name</xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-value</xsl:with-param>
                    <xsl:with-param name="value"><xsl:value-of select="../Service/@longName"/></xsl:with-param>
                </xsl:call-template>
            </value>
        </complex-instance>
        <complex-instance name="custom-metadata">
            <value>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-key</xsl:with-param>
                    <xsl:with-param name="value">short-name</xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-value</xsl:with-param>
                    <xsl:with-param name="value"><xsl:value-of select="../Service/@shortName"/></xsl:with-param>
                </xsl:call-template>
            </value>
        </complex-instance>
        <complex-instance name="custom-metadata">
            <value>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-key</xsl:with-param>
                    <xsl:with-param name="value">start-date-xml</xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-value</xsl:with-param>
                    <xsl:with-param name="value" select="Period/@start"/>
                </xsl:call-template>
            </value>
        </complex-instance>
        <complex-instance name="custom-metadata">
            <value>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-key</xsl:with-param>
                    <xsl:with-param name="value">end-date-xml</xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name='string'>
                    <xsl:with-param name="name">custom-value</xsl:with-param>
                    <xsl:with-param name="value" select="Period/@end"/>
                </xsl:call-template>
            </value>
        </complex-instance>
    </xsl:template>

    <xsl:template match="Programme">
        <xsl:param name='channelId'/>
        <xsl:choose>
            <xsl:when test="(EpgDescription/EpgElement[@key='EpisodeNumber'] and floor(EpgDescription/EpgElement[@key='EpisodeNumber']) = EpgDescription/EpgElement[@key='EpisodeNumber']) or substring(EpgDescription/EpgElement[@key='EpisodeNumber'],1,2) = 'Ep'">
                <xsl:call-template name='asset'>
                    <xsl:with-param name='assetType'>episode</xsl:with-param>
                    <xsl:with-param name='channelId'>
                        <xsl:value-of select="$channelId"/>
                    </xsl:with-param>
                    <xsl:with-param name='isEPG'>1</xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name='asset'>
                    <xsl:with-param name='assetType'>movie</xsl:with-param>
                    <xsl:with-param name='channelId'>
                        <xsl:value-of select="$channelId"/>
                    </xsl:with-param>
                    <xsl:with-param name='isEPG'>1</xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="Assets">
        <xsl:copy-of select="node()"/>
    </xsl:template>
    <!--
    MAIN TEMPLATES
    -->
    <xsl:template name="vod">
        <xsl:apply-templates select="ScheduleProvider/VodItem[EpgDescription/EpgElement[@key = 'InternetVod' and text() = 'true']]"/>
    </xsl:template>

    <xsl:template name="products">
        <xsl:apply-templates select="ScheduleProvider/Product"/>
    </xsl:template>

    <xsl:template name="catalogue">
        <xsl:apply-templates select="ScheduleProvider/Catalogue"/>
    </xsl:template>

    <xsl:template name="live-channel">
        <xsl:apply-templates select="ScheduleProvider/DownloadPeriod"/>
    </xsl:template>

    <xsl:template name="copy-assets">
        <xsl:apply-templates select="Assets"/>
    </xsl:template>

    <xsl:template match="/">
        <assets>
            <xsl:call-template name="products"/>
            <xsl:call-template name="catalogue"/>
            <xsl:call-template name="vod"/>
            <xsl:call-template name="live-channel"/>
            <xsl:call-template name="copy-assets"/>
        </assets>
    </xsl:template>

</xsl:stylesheet>
